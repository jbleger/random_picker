
# Install/upgrade

```bash
pip install --upgrade random_picker --extra-index-url https://forgemia.inra.fr/api/v4/projects/8055/packages/pypi/simple
```

# Contributing

Install pre-commit

```bash
pip install pre-commit
```

Initialize pre-commit in the repo

```bash
pre-commit install
```

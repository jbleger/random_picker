# Copyright 2023, Université de Technologie de Compiègne, France,
#                 Jean-Benoist Leger <jbleger@hds.utc.fr>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the “Software”), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# pylint: disable=missing-module-docstring

import argparse
import sys

from random_picker import choices


def _positiveint(raw_value):
    value = int(raw_value)
    if value <= 0:
        raise ValueError
    return value


def _parser():  # pylint: disable=missing-function-docstring
    parser = argparse.ArgumentParser(
        prog="random-picker",
        description="Pick randomly one line of a fixed number of line.",
    )

    parser.add_argument(
        "-n",
        "--number-of-lines",
        dest="number",
        help="Number of lines to pick. (Default: 1)",
        type=_positiveint,
        default=1,
    )
    parser.add_argument(
        "-r",
        "--with-replacements",
        help="Randomly pick with replacement (Default: without replacement)",
        dest="replacement",
        action="store_true",
    )
    parser.add_argument(
        "-s",
        "--seed",
        dest="seed",
        help="Seed the pseudo-random-generator with a integer. "
        "By default initialized with unpredictable entropy pulled from the OS.",
        type=int,
        default=None,
    )
    parser.add_argument(
        "input",
        help="Input file. (Default: standard input)",
        nargs="?",
        type=argparse.FileType("r"),
        default=sys.stdin,
    )
    parser.add_argument(
        "output",
        help="Output file. (Default: standard output)",
        nargs="?",
        type=argparse.FileType("w"),
        default=sys.stdout,
    )

    return parser


def main():  # pylint: disable=missing-function-docstring
    parser = _parser()

    args = parser.parse_args()
    args.output.write(
        "".join(
            choices(
                values=args.input,
                k=args.number,
                seed=args.seed,
                replacement=args.replacement,
            )
        )
    )

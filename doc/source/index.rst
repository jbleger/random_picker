.. Random Picker documentation master file, created by
   sphinx-quickstart on Thu Apr 27 16:28:30 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Random Picker's documentation!
=========================================

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   ./modules.rst
   ./cli.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
